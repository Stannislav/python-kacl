# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased
### Added
- Just unreleased stuff

## 1.0.0 - 2017-06-20
### Added
- OSEPD-9: issue `style 1` and another one `style 2` as well as **bold** and _italic_ must work as well
- [OSEPD-10] issue style 2
- [OSEPD-11]: issue style 3
- issue [OSEPD-12] issue style 4
- issue OSEPD-13 issue style 5